#!/bin/bash

# exit immediately if any command returns non-zero exit code
set -e

# print commands as they are executed by the shell interpreter
set -x

# list of upstream tests to run
CURL_TESTS="314 315 316"

# obtain source RPM of curl as there could be more than one
# url, use head to pick the first one
SRPM_URL=$(yum repoquery -q --srpm curl --location | head -n 1)
SRPM=${SRPM_URL##*/}
curl -Lo "$SRPM" "$SRPM_URL"

# install build-time deps
yum -y builddep "$SRPM"

# extract source RPM
rpmdev-extract "$SRPM"
cd "${SRPM%.rpm}"

# local build of curl
rpmbuild --nodeps -bc curl.spec \
    --define "_sourcedir $PWD"  \
    --define "_builddir $PWD"

# local build of upstream tests
cd curl-*/build-full/tests

# https://bugzilla.redhat.com/show_bug.cgi?id=2043092
export RPM_ARCH=$(uname -m)
export RPM_PACKAGE_RELEASE=2
export RPM_PACKAGE_VERSION=8.9.1
export RPM_PACKAGE_NAME=curl

make -j$(nproc)

# run selected upstream tests
srcdir=../../tests perl -I../../tests ../../tests/runtests.pl -a -p -v $CURL_TESTS
